/**
 * 
 */
package com.hcl.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.Employee;
import com.hcl.service.EmployeeService;

/**
 * EmployeeController
 *
 */
@RestController
public class EmployeeController {

	private static final Logger logger = Logger.getLogger(EmployeeController.class);

	@Autowired
	EmployeeService employeeService;

	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String display() {
		logger.info("this is controller");
		return "hello world";

	}

	@RequestMapping(value = "/employee", method = RequestMethod.GET, produces = "application/json")
	public List<Employee> getAllEmployees() {
		return employeeService.getAllEmployees();

	}

	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET, produces = "application/json")
	public Employee getEmployee(@PathVariable Long id) {
		return employeeService.getEmployee(id);

	}

	@RequestMapping(value = "/createemployee", method = RequestMethod.POST, consumes = "application/json")
	public void createEmployee(@RequestBody Employee employee) {
		employeeService.createEmployee(employee);
	}

	/*
	 * @RequestMapping(value = "/displayemployee/{id}", method =
	 * RequestMethod.GET, produces = "application/json") public Employee
	 * displayEmployee(@PathVariable Long id) { return
	 * employeeService.displayEmployee(id);
	 * 
	 * }
	 */

}
