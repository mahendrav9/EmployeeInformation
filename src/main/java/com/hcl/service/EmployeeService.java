package com.hcl.service;

import java.util.List;

import com.hcl.model.Employee;

/*
 * EmployeeService
 */
public interface EmployeeService {

	public List<Employee> getAllEmployees();

	public Employee getEmployee(Long id);

	public void createEmployee(Employee employee);

}
