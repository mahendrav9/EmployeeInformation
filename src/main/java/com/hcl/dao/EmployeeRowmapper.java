/**
 * 
 */
package com.hcl.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import com.hcl.model.Address;
import com.hcl.model.Employee;

/**
 * EmployeeRowmapper
 *
 */
public class EmployeeRowmapper implements ResultSetExtractor<Employee> {

	/*public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee employee = new Employee();
		employee.setEmpId(rs.getLong("emp_id"));
		employee.setEmpName(rs.getString("emp_name"));
		employee.setEmpDesignation(rs.getString("emp_designation"));
		employee.setEmpDOJ(rs.getDate("emp_doj"));

		
		List<Address> addresses = new ArrayList<Address>(rowNum);

		for (Address address : addresses) {
			Address addres = new Address();
			addres.setAddId(rs.getLong("add_id"));
			addres.setStreet(rs.getString("street"));
			addres.setCity(rs.getString("city"));
			addres.setZipCode(rs.getString("zipcode"));
			addres.setEmpId(rs.getLong("employee_id"));
			addresses.add(addres);
		}
		employee.setAddress(addresses);

		return employee;
	}
*/
	public Employee extractData(ResultSet rs) throws SQLException, DataAccessException {

		List<Address> addresses = new ArrayList<Address>();
		Employee employee = new Employee() ;
		while (rs.next()) {

			employee.setEmpId(rs.getLong("emp_id"));
			employee.setEmpName(rs.getString("emp_name"));
			employee.setEmpDesignation(rs.getString("emp_designation"));
			employee.setEmpDOJ(rs.getDate("emp_doj"));

			Address addres = new Address();
			addres.setAddId(rs.getLong("add_id"));
			addres.setStreet(rs.getString("street"));
			addres.setCity(rs.getString("city"));
			addres.setZipCode(rs.getString("zipcode"));
			addres.setEmpId(rs.getLong("employee_id"));
			addresses.add(addres);

		}
		employee.setAddress(addresses);
		return employee;
	}
	
}
