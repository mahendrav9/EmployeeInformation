/**
 * 
 */
package com.hcl.dao;

import java.util.List;

import com.hcl.model.Employee;

/**
 * EmployeeDAO
 *
 */
public interface EmployeeDAO {
	
	public List<Employee> getAllEmployees();

	public Employee getEmployee(Long id);

	public void createEmployee(Employee employee);


}
