/**
 * 
 */
package com.hcl.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hcl.model.Address;
import com.hcl.model.Employee;

/**
 * EmployeeDAOImpl
 *
 */
@Repository
public class EmployeeDAOImpl implements EmployeeDAO {

	@Autowired
	JdbcTemplate jdbcTemplate;

	/*public List<Employee> getAllEmployees() {
		return jdbcTemplate.query("SELECT * FROM employee_information", new EmployeeRowmapper());
	}*/

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Employee getEmployee(Long empid) {

		/*
		 * return (Employee) jdbcTemplate.queryForObject(
		 * "SELECT * FROM employee_information where emp_id = ? ", new Object[]
		 * { empid }, new EmployeeRowmapper());
		 */

		String employee = "SELECT emp.emp_id,emp.emp_name,emp.emp_designation,emp.emp_doj ,addr.add_id,addr.street,addr.city,addr.zipcode,addr.employee_id  "
				+ "from employee_information emp left outer join  emp_address addr on emp.emp_id=addr.employee_id where emp.emp_id=?";

		return (Employee) jdbcTemplate.query(employee, new Object[] { empid }, new EmployeeRowmapper());
	}

	public void createEmployee(Employee employee) {

		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");

		java.sql.Date requestedOn = null;
		try {
			requestedOn = new java.sql.Date(formatter.parse(employee.getEmpDOJ().toString()).getTime());

		} catch (ParseException e) {
			requestedOn = new java.sql.Date(System.currentTimeMillis());
		}

		String empinfo = "INSERT INTO employee_information(emp_id,emp_name,emp_designation,emp_doj) VALUES (?,?,?,?)";

		jdbcTemplate.update(empinfo, employee.getEmpId(), employee.getEmpName(), employee.getEmpDesignation(),
				requestedOn);

		String addresinfo = "INSERT INTO emp_address(add_id,street,city,zipcode,employee_id) VALUES (?,?,?,?,?)";
		for (Address address : employee.getAddress()) {

			jdbcTemplate.update(addresinfo, address.getAddId(), address.getStreet(), address.getCity(),
					address.getZipCode(), address.getEmpId());

		}

	}

	public List<Employee> getAllEmployees() {
		return null;
	}

}
