package com.hcl.config;

import java.util.Arrays;

public class RomanNumbers {

	public static final int[] decimal = { 1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000 };
	public static final String[] letters = { "I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M" };

	public static String stringToRoman(int num) {
		String roman = "";

		if (num < 1 || num > 3000) {
			System.out.println("Invalid roman number value!");
		}

		while (num > 0) {
			int maxFound = 0;
			for (int i = 0; i < decimal.length; i++) {
				if (num >= decimal[i]) {
					maxFound = i;
				}
			}
			roman += letters[maxFound];
			num -= decimal[maxFound];
		}

		return roman;
	}

	private static boolean isPandigital(String s) {
		if (s.length() != 9)
			return false;
		char[] temp = s.toCharArray();
		Arrays.sort(temp);
		return new String(temp).equals("1234");
	}

	public static void main(String args[]) {

		System.out.println(RomanNumbers.stringToRoman(1445));
		System.out.println(RomanNumbers.isPandigital("1445"));
	}
}
